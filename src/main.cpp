
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <memory>
#include <cstdlib>

#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <camera_info_manager/camera_info_manager.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <cv_bridge/cv_bridge.h>
#include <boost/assign/list_of.hpp>
#include <boost/thread/thread.hpp>
#include <boost/thread/sync_queue.hpp>


#include "CqUsbCam.h"
#include "SensorCapbablity.h"

string sensor = "MT9V034";
unsigned int g_width=752;
unsigned int g_height=480;

pthread_mutex_t mutexDisp;
pthread_mutex_t mutexCam;

cv::Mat curFrame;

sensor_msgs::ImagePtr msg;
sensor_msgs::CameraInfo cam_info_msg;
std_msgs::Header header;

image_transport::CameraPublisher pub;
std::shared_ptr<camera_info_manager::CameraInfoManager> cam_info_manager;


double frequency = 20;

CCqUsbCam cam0;

sensor_msgs::CameraInfo get_default_camera_info_from_image(sensor_msgs::ImagePtr img){
    sensor_msgs::CameraInfo cam_info_msg;
    cam_info_msg.header.frame_id = img->header.frame_id;
    // Fill image size
    cam_info_msg.height = img->height;
    cam_info_msg.width = img->width;
//    ROS_INFO_STREAM("The image width is: " << img->width);
//    ROS_INFO_STREAM("The image height is: " << img->height);
    // Add the most common distortion model as sensor_msgs/CameraInfo says
    cam_info_msg.distortion_model = "fisheye";
    // Don't let distorsion matrix be empty
    cam_info_msg.D.resize(5, 0.0);
    // Give a reasonable default intrinsic camera matrix
    cam_info_msg.K = boost::assign::list_of(1.0) (0.0) (img->width/2.0)
                                           (0.0) (1.0) (img->height/2.0)
                                           (0.0) (0.0) (1.0);
    // Give a reasonable default rectification matrix
    cam_info_msg.R = boost::assign::list_of (1.0) (0.0) (0.0)
                                            (0.0) (1.0) (0.0)
                                            (0.0) (0.0) (1.0);
    // Give a reasonable default projection matrix
    cam_info_msg.P = boost::assign::list_of (1.0) (0.0) (img->width/2.0) (0.0)
                                            (0.0) (1.0) (img->height/2.0) (0.0)
                                            (0.0) (0.0) (1.0) (0.0);
    return cam_info_msg;
}

void Disp(void* frameData)
{
	pthread_mutex_lock(&mutexDisp);
	curFrame = cv::Mat(g_height, g_width, CV_8UC1, (unsigned char*)frameData);	
	//std::cout<< curFrame.cols <<" "<<  curFrame.rows << std::endl;

	msg = cv_bridge::CvImage(header, "mono8", curFrame).toImageMsg();
	// Create a default camera info if we didn't get a stored one on initialization

	//ROS_WARN_STREAM("No calibration file given, publishing a reasonable default camera info.");
	cam_info_msg = get_default_camera_info_from_image(msg);
	cam_info_manager->setCameraInfo(cam_info_msg);
	
	// The timestamps are in sync thanks to this publisher
	ros::Time ts = ros::Time::now();
	ROS_INFO_STREAM("The image ts: " << ts);
	pub.publish(*msg, cam_info_msg, ts);

	double sleep = 1000.0/ frequency;
	//std::cout<<"sleel"<<sleep<<std::endl;
	cv::imshow("disp", curFrame);
	cv::waitKey(sleep);
	pthread_mutex_unlock(&mutexDisp);

}


void checkspeed()
{
	unsigned int speed = 0;
	cam0.GetUsbSpeed(speed);
	if(speed==LIBUSB_SPEED_SUPER)
	{
		printf("USB 3.0 device found on cam0!\n");
		cam0.SendUsbSpeed2Fpga(LIBUSB_SPEED_SUPER);
	}
	else if(speed==LIBUSB_SPEED_HIGH)
	{
		printf("USB 2.0 device found on cam0!\n");
		cam0.SendUsbSpeed2Fpga(LIBUSB_SPEED_HIGH);
	}
	else
	{
		printf("Device speed unknown on cam0!\n");
	}

}

int main(int argc, char *argv[])
{

	if (argc != 2) {
		std::cout<<"Usage: camera_publisher publish_Hz \n "
				"Example: camera_publisher 20" <<std::endl;
		return -1;
	}

	frequency = (double)atoi(argv[1]);
	std::cout <<"Frequency: " << frequency<<std::endl;


	cq_int32_t ret;
	ret =pthread_mutex_init(&mutexDisp, NULL);
	if(ret!=0)
		printf("pthread_mutex_init failed");
	ret =pthread_mutex_init(&mutexCam, NULL);
	if(ret!=0)
		printf("pthread_mutex_init failed");

	cam0.SelectSensor(sensor);

	int usbCnt=CCqUsbCam::OpenUSB();
	printf("%d usb device(s) found!\n", usbCnt);
	if(usbCnt<=0)
	{
		printf("exiting ...\n");
		return -1;
	}
	cam0.ClaimInterface(0);
	//cam0.InitSensor(); //not required by 	mt9v034

	checkspeed();

	ros::init(argc, argv, "image_publisher");
    ros::NodeHandle nh;
    ros::NodeHandle _nh("~"); // to get the private params
    image_transport::ImageTransport it(nh);
    pub = it.advertiseCamera("Camera0", 1);

	cam_info_manager = std::make_shared<camera_info_manager::CameraInfoManager>(nh, "Camera0", "");
    // Get the saved camera info if any
	cam_info_msg = cam_info_manager->getCameraInfo();

    header.frame_id = "Camera0";
	cam0.StartCap(g_height,  g_width, Disp);


	pthread_mutex_lock(&mutexCam);
	ros::Rate r(30);
	while (nh.ok()) {
		ros::spinOnce();
		r.sleep();
	}
	cam0.StopCap();

	pthread_mutex_unlock(&mutexCam);

	cam0.ReleaseInterface();
	CCqUsbCam::CloseUSB();

	printf("Exiting ...\n");
	exit(0);

	return 0;
}



